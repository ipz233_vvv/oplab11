#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
int main() {
	float min[] = { 0, -4, 100, -35, -128,  7,           -7.85, -100, 23,0,sqrt(17) };
	float max[] = { 0, -1, 299,  -1,  127, 13, 28 * sqrt(3), 100, 71,2,sqrt(82) };
	int upbound[] = {0,1,0,0,0,1,0,0,0,1,1 };
	int type[] = {0,0,0,1,0,2,0,0,0,0,0};
	srand(time(NULL));
	for (int i = 1; i <= 10; i++) {
		printf("%2d) [%3.7f, %3.7f", i, min[i], max[i]);
		printf(upbound[i] == 0 ? "]" : ")");
		printf(type[i] == 0 ? "" : type[i] == 1 ? " only even" : " only odd");
		printf("\n");
	}
	while (true) {
		int sel;
		scanf("%d", &sel);
		if (sel == 0)
			break;
		int random = 0;
		while (true) {
			random = (rand() * 1. / (RAND_MAX)) * (max[sel] - min[sel]) + min[sel];
			if (upbound[sel]==1 && random == max[sel])
				continue;
			if (type[sel] == 1 && abs(random) % 2 == 1)
				continue;
			if (type[sel] == 2 && abs(random % 2 == 0))
				continue;
			break;
		}
		printf("Random value from %f to %f is %d\nSelect generator or enter 0 to exit: ",min[sel],max[sel], random);
	}
	return 0;
}