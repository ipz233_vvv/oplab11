#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
int main() {
	int m, n, k;
	srand(time(NULL));
	printf("Enter m,n,k:\n");
	scanf("%d%d%d", &m, &n, &k);
	for (int i = 0; i < m + n; i++) {
		for (int j = 0; j < k; j++) {
			if (i < m)
				printf("%5d  ", rand() % 300 - 200);
			else
				printf("%.3lf  ", rand() % 10001 / 1000.0);
		}
		printf("\n");
	}
	return 0;
}