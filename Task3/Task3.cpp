#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
int main() {
	srand(time(NULL));
	int n;
	int a,b;
	int m[100000];
	printf("Enter n,a,b:\n");
	scanf("%d", &n);
	scanf("%d%d", &a,&b);
	long long d = 1;
	int start0 = -1, end0 = -1;
	for (int i = 0; i < n; i++)
	{
		m[i] = a+rand()%(b-a);
		printf("%d ", m[i]);

		if (m[i] % 2 == 0)
			d *= m[i];
		if (m[i] == 0) {
			end0 = i;
			if (start0 == -1)
				start0 = i;
		}
	}
	int s = 0;
	printf("\n");
	if (start0 != end0)
	{
		for (int i = start0; i < end0; i++) {
			s += m[i];
		}
		printf("Sum between zeros: %d\n", s);
	}
	else
	{
		printf("There is no zeros or numbers between zeros\n");
	}
	if (d == 1)
		d = 0;
	printf("Product of even numbers: %ld\n", d);
	return 0;
}